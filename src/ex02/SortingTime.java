package ex02;

import java.util.Random;

public class SortingTime {

	private int[] array;
	private int[] tempMergeArr;
	private int length;

	public static void main(String[] args) {
		Random rng = new Random();
		int size = 100000;
		int[] arrayOne = new int[size];
		int[] arrayTwo = new int[size];
		int[] arrayThree = new int[size];

		for (int i = 0; i < size; i++) {
			int random = rng.nextInt(100000);
			arrayOne[i] = random;
			arrayTwo[i] = random;
			arrayThree[i] = random;
		}

		long startTimeMerge = System.nanoTime();
		SortingTime mergeSort = new SortingTime();
		mergeSort.sort(arrayOne);
		long estimatedTimeMerge = System.nanoTime() - startTimeMerge;

		System.out.println(
				"The time needed to sort with merge sort is: " + (estimatedTimeMerge / 1000000D) + " miliseconds.");

		long startTimeQuick = System.nanoTime();
		quickSortInPlace(arrayTwo, 0, 99999);
		long estimatedTimeQuick = System.nanoTime() - startTimeQuick;

		System.out.println(
				"The time needed to sort with quick sort is: " + (estimatedTimeQuick / 1000000D) + " miliseconds.");

		long startTimeBubble = System.nanoTime();
		bubbleSort(arrayThree);
		long estimatedTimeBubble = System.nanoTime() - startTimeBubble;

		System.out.println(
				"The time needed to sort with bubble sort is: " + (estimatedTimeBubble / 1000000D) + " miliseconds.");
	}

	public static void bubbleSort(int[] arrayThree) {

		int n = arrayThree.length;
		int temp = 0;

		for (int i = 0; i < n; i++) {
			for (int j = 1; j < (n - i); j++) {

				if (arrayThree[j - 1] > arrayThree[j]) {
					temp = arrayThree[j - 1];
					arrayThree[j - 1] = arrayThree[j];
					arrayThree[j] = temp;
				}

			}
		}
	}

	private static void quickSortInPlace(int[] arrayTwo, int a, int b) {
		if (a >= b)
			return;

		int left = a;
		int right = b - 1;
		int pivot = arrayTwo[b];
		int temp;

		while (left <= right) {
			while (left <= right && arrayTwo[left] < pivot)
				left++;
			while (left <= right && arrayTwo[right] > pivot)
				right--;
			if (left <= right) {
				temp = arrayTwo[left];
				arrayTwo[left] = arrayTwo[right];
				arrayTwo[right] = temp;
				left++;
				right--;
			}
		}
		temp = arrayTwo[left];
		arrayTwo[left] = arrayTwo[b];
		arrayTwo[b] = temp;
		quickSortInPlace(arrayTwo, a, left - 1);
		quickSortInPlace(arrayTwo, left + 1, b);
	}

	public void sort(int[] arrayOne) {
		this.array = arrayOne;
		this.length = arrayOne.length;
		this.tempMergeArr = new int[length];
		doMergeSort(0, length - 1);
	}

	private void doMergeSort(int lowerIndex, int higherIndex) {

		if (lowerIndex < higherIndex) {
			int middle = lowerIndex + (higherIndex - lowerIndex) / 2;
			doMergeSort(lowerIndex, middle);
			doMergeSort(middle + 1, higherIndex);
			mergeParts(lowerIndex, middle, higherIndex);
		}
	}

	private void mergeParts(int lowerIndex, int middle, int higherIndex) {

		for (int i = lowerIndex; i <= higherIndex; i++) {
			tempMergeArr[i] = array[i];
		}
		int i = lowerIndex;
		int j = middle + 1;
		int k = lowerIndex;
		while (i <= middle && j <= higherIndex) {
			if (tempMergeArr[i] <= tempMergeArr[j]) {
				array[k] = tempMergeArr[i];
				i++;
			} else {
				array[k] = tempMergeArr[j];
				j++;
			}
			k++;
		}
		while (i <= middle) {
			array[k] = tempMergeArr[i];
			k++;
			i++;
		}

	}
}

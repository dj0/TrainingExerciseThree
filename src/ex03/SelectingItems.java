package ex03;

import java.util.Random;

public class SelectingItems {

	public static void main(String[] args) {
		Random rng = new Random();

		int[] randomArray = new int[1000000];
		for (int i = 0; i < randomArray.length; i++) {
			randomArray[i] = rng.nextInt(1000000);
		}

		for (int j = 10000; j <= 50000;) {
			if (j == 10000) {
				long startTime10k = System.nanoTime();
				System.out.println(findKthLargest(randomArray, j));
				long estimatedTime10k = System.nanoTime() - startTime10k;
				System.out.println("The time needed to get the 10k element is: " + (estimatedTime10k / 1000000D)
						+ " miliseconds.");
				j += 10000;
			} else if (j == 50000) {
				long startTime50k = System.nanoTime();
				System.out.println(findKthLargest(randomArray, j));
				long estimatedTime50k = System.nanoTime() - startTime50k;
				System.out.println("The time needed to get the 50k element is: " + (estimatedTime50k / 1000000D)
						+ " miliseconds.");
				j += 10000;
			} else {
				System.out.println(findKthLargest(randomArray, j));
				j += 10000;
			}
		}

		for (int k = 100000; k <= 500000;) {
			if (k == 500000) {
				long startTime500k = System.nanoTime();
				System.out.println(findKthLargest(randomArray, k));
				long estimatedTime500k = System.nanoTime() - startTime500k;
				System.out.println("The time needed to get the 500k element is: " + (estimatedTime500k / 1000000D)
						+ " miliseconds.");
				k += 50000;
			} else {
				System.out.println(findKthLargest(randomArray, k));
				k += 50000;
			}
		}

		for (int l = 600000; l <= 1000000;) {
			if (l == 1000000) {
				long startTime1m = System.nanoTime();
				System.out.println(findKthLargest(randomArray, l - 1));
				long estimatedTime1m = System.nanoTime() - startTime1m;
				System.out.println(
						"The time needed to get the 1m element is: " + (estimatedTime1m / 1000000D) + " miliseconds.");
				l += 100000;
			} else {
				System.out.println(findKthLargest(randomArray, l));
				l += 100000;
			}
		}
	}

	public static int findKthLargest(int[] nums, int k) {
		int start = 0, end = nums.length - 1, index = k;

		while (start < end) {
			int pivot = partion(nums, start, end);

			if (pivot < index)
				start = pivot + 1;
			else if (pivot > index)
				end = pivot - 1;
			else
				return nums[pivot];
		}
		return nums[start];
	}

	private static int partion(int[] nums, int start, int end) {
		int pivot = start;
		int temp;

		while (start <= end) {
			while (start <= end && nums[start] <= nums[pivot])
				start++;
			while (start <= end && nums[end] > nums[pivot])
				end--;

			if (start > end)
				break;

			temp = nums[start];
			nums[start] = nums[end];
			nums[end] = temp;
		}

		temp = nums[end];
		nums[end] = nums[pivot];
		nums[pivot] = temp;

		return end;
	}
}

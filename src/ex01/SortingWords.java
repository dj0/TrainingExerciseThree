package ex01;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;

public class SortingWords {

	public static ArrayList<String> filter(String line) {
		String[] words = line.toLowerCase().split(" ");
		ArrayList<String> list = new ArrayList<>(Arrays.asList());

		for (int i = 0; i < words.length; i++) {
			words[i] = words[i].replace(".", "");
			words[i] = words[i].replace(",", "");
			words[i] = words[i].replace("!", "");
			words[i] = words[i].replace("?", "");
			words[i] = words[i].replace(":", "");
			words[i] = words[i].replace(";", "");
			words[i] = words[i].replace("`", "");
			words[i] = words[i].replace("'", "");
			words[i] = words[i].replace("\r\n", "");
			words[i] = words[i].replace("\n", "");
			words[i] = words[i].replace("\t", "");
			words[i] = words[i].replace("-", "");
			words[i] = words[i].replace("(", "");
			words[i] = words[i].replace(")", "");
		}

		for (String word : words) {
			list.add(word);
		}

		return list;
	}

	private static void quickSortInPlace(String[] arrayToSort, int a, int b) {
		if (a >= b)
			return;

		int left = a;
		int right = b - 1;
		String pivot = arrayToSort[b];
		String temp;

		while (left <= right) {
			while (left <= right && arrayToSort[left].length() < pivot.length())
				left++;
			while (left <= right && arrayToSort[right].length() > pivot.length())
				right--;
			if (left <= right) {
				temp = arrayToSort[left];
				arrayToSort[left] = arrayToSort[right];
				arrayToSort[right] = temp;
				left++;
				right--;
			}
		}
		temp = arrayToSort[left];
		arrayToSort[left] = arrayToSort[b];
		arrayToSort[b] = temp;
		quickSortInPlace(arrayToSort, a, left - 1);
		quickSortInPlace(arrayToSort, left + 1, b);
	}

	public static void main(String[] args) {
		Path path = Paths.get("sample_text.txt");
		ArrayList<String> wordsList = new ArrayList<>();

		try {
			BufferedReader reader = Files.newBufferedReader(path);
			String line = null;

			while ((line = reader.readLine()) != null) {
				wordsList.addAll(filter(line));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		String[] arrayToSort = new String[wordsList.size()];
		arrayToSort = wordsList.toArray(arrayToSort);

		quickSortInPlace(arrayToSort, 0, 200);
		for (int j = 0; j < 200; j++) {
			System.out.println(arrayToSort[j]);
		}
	}
}
